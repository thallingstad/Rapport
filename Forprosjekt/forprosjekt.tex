\documentclass[norsk]{article}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[norsk]{babel}
\usepackage{parskip}
\usepackage{verbatim}
\usepackage{float}
\usepackage{csquotes}
\usepackage{colortbl}
\usepackage[dvipsnames]{xcolor}
\selectlanguage{norsk}
\graphicspath{ {figurer/} }
\title{Reproduserbare testinfrastrukturer}
\author{Tobias Hallingstad}


\begin{document}
\begin{titlepage}


%----------------------------------------------------------------------------------------
%	FORSIDE
%----------------------------------------------------------------------------------------


\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page
 
\includegraphics[scale=0.4]{fihlogo}

\textsc{\LARGE Forsvarets ingeniørhøgskole}\\[1.5cm] % Name of your university/college
\textsc{\Large Forprosjekt til}\\[0.5cm] % Major heading such as course name
\textsc{\Large Hovedoppgave}\\[0.5cm] % Minor heading such as course title


\HRule \\[0.4cm]
{ \huge \bfseries Reproduserbare testinfrastrukturer}\\[0.4cm] % Title of your document
\HRule \\[1.5cm]
 
\Large \emph{Student:}\\
Tobias \textsc{Hallingstad}\\[3cm] % Your name

%----------------------------------------------------------------------------------------
%	DATE SECTION
%----------------------------------------------------------------------------------------

{\large \today}\\[3cm] % Date, change the \today to a set date if you want to be precise


\vfill % Fill the rest of the page with whitespace

\end{titlepage}


\maketitle

\begin{abstract}
Mye tid kan spares på å gjennomføre testing på en effektiv måte. Dette forprosjektet legger til rette for en hovedoppgave hvor det skal utarbeides en prototype som automatiserer oppsett og administrasjon av testkonfigurasjoner. Det innebærer deployering av operativsystemer med programvare og konfigurasjon, oppsett rutere og svitsjer, og en metode for å holde styr på versjoner og endringer av hvert testmiljø. Som en del av tilretteleggingen er en rekke eksisterende verktøy som kan nyttes til deler av dette undersøkt. Et utvalg av de introduseres i teoridelen. Metodene som nyttes for å utføre prosjektet omfatter både utvikling av programvare, fordypning i teori og rapportskriving med støtte av veiledere.
\end{abstract}

\tableofcontents
\newpage
\section{Introduksjon}

\subsection{Bakgrunn}
Testing er en viktig og tidkrevende prosess som det alltid vil være behov for under utvikling ny programvare, nye metoder og lignende. Grundig testing er nødvendig siden mennesker gjør feil, spesielt når det angår datanettverksinfrastruktur som raskt blir komplekse systemer sammensatt ulike plattformer og nettverksutstyr med ulik programvare. I et slikt system, med mange påvirkningsforhold, lar det seg sjeldent gjøre å forutsi med sikkerhet den totale virkningen av en handling på systemet eller hvordan de ulike komponentene vil virke sammen.

Det er viktig å sette opp et godt testmiljø slik at testingen kan være rettet mot virkeligheten. Det involverer gjerne at de samme enhetene, operativsystemene og programvarene benyttes. Et testmiljø har derfor typisk krav som begrenser det til en spesifikk case. Som følge av dette settes det opp en rekke ulike testmiljøer, og det må holdes styr på alle de ulike konfigurasjonene som benyttes. 

Testmiljøene kan settes opp med alle komponentene fysisk, være helt virtuelle på en maskin eller en mellomting med både virtualisering og flere fysiske komponenter.

\subsection{Problembeskrivelse}
I forbindelse med feilsøking, test og utvikling er det behov for å kjøre ulike programvarekonfigurasjoner og kode på en rekke forskjellige plattformer. Dette blir fort utfordrende dersom hele økosystemer av infrastruktur, operativsystem og programvare skal testes sammen.

For å kunne gjennomføre reproduserbare tester med ulike plattformer og infrastrukturer effektivt, er det nødvendig å ha deployeringsmekanismer for å sette opp hele testinfrastrukturer basert på en konfigurasjon med versjonskontroll.

\subsection{Begrunnelse}
Ettersom testing er en sentral del av mye av det som gjøres i IT-bransjen, vil metoder som automatiserer oppsett og vedlikehold av testmiljøer kunne bespare mye tid. Automatisering vil også bidra til at testmiljøene settes opp likt hver gang slik at menneskelige feil kan unngås under oppsett.

I denne oppgaven skal det undersøkes eksisterende verktøy og rammeverk som kan brukes til dette. På bakgrunn av undersøkelsen skal det gjøres en inndeling i hvilke arkitekturer som kan benyttes. Dette skal danne et grunnlag for å utvikle en prototype som dekker behovet problembeskrivelsen brakte frem.


\subsection{Forskningsspørsmål}
\label{fs}
Siden studien skal underbygge utviklingen av en prototype, vil disse kravene til løsning brukes som grunnlag for mine forskningsspørsmål:

Prototypen skal


\begin{enumerate}
\item kunne ta parametere som inngangsverdier og deretter automatisk sette virtuelle maskiner med alle nødvendige instillinger og programvarer.
\item ha testkonfigurasjoner i et versjonskontrollsystem med mulighet for tilbakerulling til tidligere versjon, utrulling av ny osv.
\item være generell, ikke låst til spesifikke plattformer som kan deployeres.
\item kunne gjøre automatisert oppsett av nettverksutstyr.

Disse kravene er ufravikelige. I tillegg, er det ønskelig at løsningen kan
\item deployere til både virtuelle og fysiske datamaskiner.
\end{enumerate}

De eksisterende løsningene som skal undersøkes behøver ikke å oppfylle alle kravene. Hensikten med studien er å undersøke hvordan hvert krav er oppfylt tidligere slik at den samlede kunnskapen gir grunnlag til å skape et produkt som oppfyller alle. Studiens forskningsspørsmål er:

\begin{enumerate}
\item Hvilke tilgjengelige rammeverk og verktøy kan benyttes til å oppfylle kravene gitt over?
\item Hvordan kan de eksisterende tilnærmingsmetodene inndeles i typer arkitektur?
\item Hvordan bør en prototype designes for å oppfylle alle de ufravikelige kravene, og eventuelt det ønskelige?
\end{enumerate}

\input{teori}

\section{Sammendrag av forventet bidrag}
Det er forventet at oppgaven vil belyse at oppsett og konfigurering av testmiljøer er en tidkrevende tidkrevende prosess som kan automatiseres.

Teoridelen introduserte en rekke verktøy som kan utføre oppgaver slik som deployering og installasjon av operativsystemer med tilpasset konfigurasjon og programvare, automatisert oppsett av rutere og svitsjer, og versjonskontroll. Alt dette kan være til hjelp for å redusere tiden som brukes til testing. Denne oppgaven vil bidra med å samle noen av verktøyene og tilnærmingene til ett produkt. Den vil komme frem til hva som er en gunstig sammensetning av løsninger basert på utfordringene og behovene problembeskrivelsen brakte frem.

Oppgaven vil med andre ord gi et mer utfyllende svar på forskningsspørsmål 3 og resultere i en prototype som baserer seg på det svaret; hva som anses som et godt design for å best mulig oppfylle de ufravikelige kravene. Det vil også undersøkes nærmere om prototypen bør avgrenses til virtuelle maskiner eller også fungere for fysiske maskiner slik som spesifisert i det ønskelige kravet. Kravene er listet i seksjon \ref{fs}.

\section{Valg av metode}
Prosessen som er valgt består av fire faser. Den første er en ren skrivebordsstudie hvor hensikten er å besvare forskningsspørsmål 1 og 2. De neste to fasene vil ta for seg utviklingen av prototypen. I den siste fasen vil det skriftlige produktet ferdigstilles og kvalitetssikres.

Oppgaven vil skrives med jevnlig konsultasjon av mentor. Det vil settes opp gjevnlige møter for å få faglig feedback på det som er gjort og innspill for fremdriften videre. I tillegg vil det sendes delleveranser til veileder ved FIH som vil kunne se oppgaven fra et annet perspektiv og gi tilbakemeldinger på det mer formelle ved oppgaveskrivingen.

\subsection{Studiefase}
Under studiefasen vil jeg lete i åpne kilder etter verktøy og rammeverk som løser hele eller deler av problemområdet. Hensikten med studien å få kunnskap om allerede eksisterende løsninger for å danne et godt grunnlag før som kan benyttes i programvareutviklingen i de senere fasene. Studien vil gi forståelse for verktøy, APIer, skript ol. som kan benyttes slik at det kan gjøres realistiske og gode valg under programdesignfasen. Denne metoden er valgt som den mest gunstige for å svare på forsknignsspørsmål 1 og 2 som vil kreve at en stor mengde informasjon tolkes, filtreres og gjøres relevant.

\subsection{Eksperimenteringsfase}
\label{ss:eksp}
I denne fasen er det satt av tid til å teste ut de verktøyene og metodene som ble funnet i forrige fase fungerer i praksis. Hensikten med denne metoden er å få en bedre forståelse av hvordan tilnærmingene fungerer og kunne oppdage vanskeligheter som ikke kom frem under den teoretiske studien.

\subsection{Utviklingsfase}
Prototypen skal utvikles i denne fasen. Prototyping er en fleksibel metode som kan benyttes på mange måter \cite[s.~115]{hasle}. Denne prototypen vil utvikles til et endelig system. Utviklingsmetoden vil hente inspirasjon fra den agile metodologien eXtreme Programming (XP). XP er tiltenkt grupper på inntil 12 personer, ikke enkeltpersoner som i dette prosjektet. De grunnleggende prinsippene modellen bygger på kan likevel benyttes. Dens fire kjerneaktiviteter er koding, testing, lytting og design. Av disse er koding og testing de viktigste \cite[s.~120]{hasle}. Kodingen og testingen gjøres i iterasjoner hvor det utvikles komponenter som tidlig integreres med de andre komponentene. Programmeringen og testingen skal føre til små ferdigstillinger med stor verdi. Det er et av XPs grunnleggende prinsipper \cite[s.~126]{hasle}. Et stort fortrinn ved å jobbe med små iterasjoner er at metoden er fleksibel og kan tåle endringer designet eller planen underveis.  Et annet av XPs prinsipper som vil benyttes i utviklingsfasen er \emph{enkel konstruksjon}. Det går hovedsakelig ut på at koden har færrest mulig klasser og metoder, er uten duplisering og kun består av det som er nødvendig \emph{nå}. 

\subsubsection{Programdesign}
Hensikten med denne fasen er å komme frem til den grunnleggende arkitekturen til programvaren som skal utvikles. Fasen vil resultere i diagrammer og beskrivelser av hvordan prototypen skal fungere på et overordnet nivå. Designet skal være en enkel konstruksjon inndelt i moduler slik at implementering og testingsfasen kan foregå iterativt.

\textbf{Planlegging vs. smidighet}\\
En omfattende innledende designfase er nærmest blasfemisk mot XP hvor koding alltid går foran bruk av modeller \cite[s.~121]{hasle}. XP kutter kraftig ned på overordnede artefakter som ikke bidrar direkte til produktet som leveres sammenlignet med de fleste andre utbredte utviklingsmetoder. Design av modeller i XP holdes derfor til et minimum og lages kun når det anses som nødvendig for implementeringen \cite[s.~131]{hasle}. For denne prototypen er samtidig modellene og designet ferdifullt i seg selv siden det skal inngå i en rapport og presenteres. 

For å bevare utviklingsmetodens smidighet skal denne fasen kun omfatte detaljer som er nødvendig for at arkitekturen skal gi mening. Det skal også være mulig å gjøre endringer på arkitekturen under implementeringsfasen. Estetisk finpuss av modellene skal derfor vente til ferdigstillingsfasen. 


\subsubsection{Implementering og testing}
Denne fasen vil foregå i iterasjoner. Det vil si at hver modul vil gjøres fullstendig før den neste skal implementeres. Hver iterasjon vil bestå av kravspesifikasjon, implementering, enhetstesting og integrasjonstesting. 


\begin{figure}[H]
\centering
\includegraphics{iterasjon}
\caption{Én iterasjon}
\centering
\end{figure}


I kravspesifikasjonen defineres hvordan modulen skal fungere i et høyere detaljnivå enn i arkitekturskissen fra programdesignfasen. Kravene som defineres skal være enkle og målbare slik at kravspesifikasjonen kan brukes som en sjekkliste under enhetstestingen. Når kravene er bestemt, starter selve implementeringen. Dette vil utgjøre den største andelen av arbeidet i denne fasen. Under enhetstesting vil modulen testes alene etter kravene fra kravspesifikasjonen. Til slutt vil modulen integreres i systemet og testes sammen med det som foreløpig er implementert.

Hver iterasjon vil ikke nødvendigvis være en helt lineær prosess. Det kan være nødvendig å gå tilbake til implementering som følge av feil som blir oppdaget under enhetstesting. Integrasjonstesting kan også avdekke at en annen modul må endres.


\subsection{Ferdigstilling}
Her skal rapporten gjennomleses og gjøres fullstendig. Det skal legges vekt på at det er sammenheng mellom kapitlene, figurer er tilstrekkelig kommentert og kildehenvisninger er korrekte. Helt til slutt gjennomføres en estetisk finpuss av rapporten med utbedring av modeller og figurer.

\section{Fremdriftsplan, leveranser og ressurser}

Det er utarbeidet en plan for gjennomføringen av oppgaven. Denne er skissert i et gantt-diagram, se figur \ref{fig:g1} og \ref{fig:g2}. Studiefasen er planlagt som en del av forprosjektet.

Perioden fra selve hovedoppgaven starter til den skal leveres har en lengde på 69 arbeidsdager. 11 dager senere skal den fremføres muntlig. I denne perioden vil det forekomme kurs og andre aktiviteter som kan føre til noe forskyvning og justering av tidsplanen. Datoene for delleveransene vil likevel holdes statiske. Rapportskrivingen vil foregå kontinuerlig gjennom alle fasene frem til innlevering 6. april. Det påregnes for eksempel at det skal settes av tid til utbedringer på bakgrunn av tilbakemelding på forprosjektet under eksperimenteringsfasen.

Planen består av fem leveranser. De obligatoriske innleveringene er forprosjektet, hovedoppgaven og det muntlige foredraget. I tillegg er det planlagt innlevering av to utkast.

Det første utkastet leveres like etter programdesignfasen. Det vil bestå av diskusjon fra eksperimenteringsfasen og skissen designfasen resulterer i. Det andre utkastet leveres tre uker etter at prototypen skal være komplett. Denne versjonen vil så langt det lar seg gjøre være fullstendig. Det vil si inneholde en tidlig versjon av alle punktene i rapporten slik at den er et grunnlag for tilbakemeldinger og innspill.

Leveransene som er planlagt er et minimum. Utover dette vil kommunikasjonen med veiledere være fri. I etterkant av tilbakemeldinger vil det gjerne være være ønske om presiseringer, levering av utbedringer, nye innspill osv. Prosjektet holdes oppdatert på GitLab. Repositoriet er ikke offentlig, men veiledere inviteres inn og kan følge fremgangen der.

Ved datoen prototypen skal være komplett, vil det sannsynligvis fortsatt være mye rom for forbedring. Det skal likevel tas en pause fra utviklingen og fokuseres på å utkast v2 av rapporten før prototypen utvikles videre. Dette skal hindre den første hendelsen i risikoanalysen (\ref{ra}) fra å inntreffe.



\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{gantter-tabell}
\caption{Gantter, del 1: Tabell}
\label{fig:g1}
\centering
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.36]{gantter-utabell}
\caption{Gantter, del 2: Diagram}
\label{fig:g2}
\centering
\end{figure}

\section{Gjennomførbarhet}
Det er forventet at prosjektet vil være godt gjennomførbart innen tidsrammene. Årsaken til det er at det er slingring både i omfanget av prototypen som skal utvikles og i mengden tid frem mot innlevering. Prototypen kan gjøres mer eller mindre kompleks og vil utvikles i moduler slik at de mest kritiske for et fungerende produkt utvikles først. Om ikke alt fungerer ved den planlagte datoen, er det ok; det er en prototype og det vil være mer tid senere. Krav nummer fem er også gjort ønskelig for å gi fleksibilitet. Det er med hensikt lagt inn mye tid til rapportskriving og ferdigstilling mot slutten. Nøyaktig hvordan denne tiden vil disponeres kan planlegges nærmere når det viser seg hva som trengs.

\section{Risikoanalyse}
\label{ra}
Risikoanalysen tar for seg noen mulige utfordringer som kan oppstå under utførelsen av prosjektet. Hensikten er å identifisere uønskede hendelser som kan oppstå for å iverksette tiltak som kan redusere sannsynligheten for og/eller konsekvensen av hendelsene inntreffer.

Denne risikoanalysen tar utgangspunkt i modellen Operational Risk Management (ORM). Risiko (R) beregnes som produktet av sannsynlighet (S) og konsekvens (K), hvor S og K tallfestes i en skala fra 1 (liten) til 5 (stor).


\renewcommand{\arraystretch}{2}
\begin{table}[H]
\caption{Risikoanalyse}
\label{my-label}
\begin{tabular}{|p{3cm}|c|c|c|p{3cm}|p{4cm}|}
\hline
\textbf{Risikobeskrivelse} & \textbf{S} & \textbf{K} & \textbf{R} & \textbf{Tiltak for å redusere risiko} & \textbf{Eventuell handling ved inntruffet hendelse} \\ \hline
Implementering av prototype stjeler tid fra rapportskriving. & 2 & 4 & 8 & Sette opp en tidsplan som sikrer god tidsbruk. & Akseptere at prototypen ikke er fullstendig, fokusere på rapport. \\ \hline
Arkitekturen er ikke gjennomførbar. & 3 & 3 & 9 & Sørge for at arkitekturen er på et overordnet nivå og kunne tåle endringer. & Redesigne arkitekturen og tilpasse prototypen. \\ \hline
Tap av data. & 1 & 4 & 4 & Sørge for å ha gode rutiner for lagring og versjonskontroll. Bruke skytjenester som GitLab og Dropbox. &  Gå tilbake til site lagrede versjon og reprodusere det tapte. \\ \hline
\end{tabular}
\end{table}

\section{Etiske og juridiske aspekt}
Det er ikke funnet noen etiske eller juridiske aspekt ved dette prosjektet.

\newpage
\input{litteratur}

\end{document}
