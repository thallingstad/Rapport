\chapter{Innledning}
\pagenumbering{arabic}

\section{Bakgrunn}
Testing er en tidkrevende prosess og en viktig del av utviklingen av ny programvare og nye metoder. Grundig testing er nødvendig siden mennesker gjør feil, spesielt når det angår data\-nett\-verks\-infra\-struk\-tur som raskt blir komplekse systemer sammensatt av ulike plattformer og nettverksutstyr med ulik programvare. Uten testing i et slikt system, med mange påvirknings\-forhold, lar det seg sjeldent gjøre å forutsi med sikkerhet den totale virkningen av en handling på systemet eller hvordan ulike komponenter vil virke sammen.

Det er viktig å sette opp gode miljøer hvor testingen kan være rettet mot virkeligheten. Det involverer gjerne at de samme enhetene, operativsystemene og programvarene benyttes. Et testmiljø har derfor typisk krav som begrenser det til et spesifikt formål. Som følge av dette er det behov for en rekke ulike testmiljøer, og det må holdes styr på alle de ulike konfigurasjonene som benyttes.

\subsection{Hva menes med testmiljø?}
\label{ss:testmiljo}
Testmiljø og testinfrastruktur er sentrale begreper som brukes om hverandre i denne oppgaven. Klarhet i hva som legges i disse begrepene er viktig for å forstå oppgavens hensikt.

Hvordan et testmiljø er sammensatt, spesifikke vertkøy og metoder for testing er ikke av interesse i seg selv i denne rapporten. Her er det likevel interessant å sette rammene for hvilken type infrastruktur prototypen skal fasilitere for å sette opp.

Programvare og metoder som testes ut har normalt en infrastruktur hvor de er ment å virke. I tilfeller hvor det er det ugunstig eller umulig å teste i denne infrastrukturen, er et testmiljø den alternative infrastrukturen som settes opp som erstatning. Den vil ofte etterligne annen infrastruktur, men bare deler som er relevante for testingen. 

Testmiljøer skal kunne dekke et stort spekter av behov. Et verktøy for håndteringen av disse bør kunne håndtere generelle systemer og ikke sette ytre begrensninger for hvilke operativsystemer, programvarer eller annen infrastruktur som kan inngå. En begrensing som ikke kan omgås gjelder testmiljøenes størrelse hvor det vil finnes en øvre grense for hva som er håndterlig.

På bakgrunn av dette gjøres det noen innledende antakelser om testmiljøer: De er minimalistiske. Det vil si mindre systemer enn typisk permanent infrastruktur, reduserte i form av at funksjonalitet som ikke er relevant for testingen unngås. Dette må ikke forveksles med at de alltid er enkle; testmiljøene kan være komplekse systemer med ulike plattformer samlet i et lite miljø. De er dynamiske på den måten at de kan forandres fortløpende ettersom nye behov oppstår. De er midlertidige og opphører sammen med formålet for testingen.

\section{Beskrivelse av problemområdet}
Å sette opp og vedlikeholde testmiljøer kan være både tidkrevende og innviklet. Det er behov for verktøy som kan deployere hele infrastrukturer, men i testsammenheng er det lite ønskelig å innføre verktøy og rammeverk som innfører mye ekstra kompleksitet.

\section{Oppgavens bidrag}
Oppgaven er et bidrag i å gjøre oppsett og vedlikehold av testinfrastruktur mindre tidkrevende og innviklet.

Bidraget skal bestå av både en studie og en prototype. Studien skal undersøke eksisterende verktøy som kan benyttes for deployering, systematisering og versjonskontroll. Det innebærer fullstendige rammeverk som gir mulighet for å deployere `fullstack'-infrastruktur, men hovedsakelig verktøy som kan utføre en delmengde av funksjonene som kreves. Det vil fremheves hvordan de ulike verktøyene skiller seg fra hverandre med tanke på arkitektur.

Prototypen baseres på resultater fra studien. Noen verktøy vil anvendes direkte, andre vil innføre konsepter og teknikker som kan anvendes. Prototypen vil være en sentralisert server hvor testkonfigurasjoner med virtuelle maskiner og nettverkskonfigurasjon kan lagres, endres og deployeres. Den vil ha mekanismer for versjonskontroll av alle komponentene av et testmiljø.

Prototypen skal være til hjelp i problemområdet som et eksempel på hvordan denne funksjonaliteten kan gjøres med enkle verktøy og grep, uten å innføre unødvendig kompleksitet. Det gjør den passende til å håndtere testinfrastruktur slik det fremkommer i seksjon \ref{ss:testmiljo}.

\section{Oppgavens oppbygning}
Denne oppgaven har to grunnleggende mål. Den skal både undersøke verktøy og rammeverk som allerede eksisterer og dokumentere utviklingen av en ny prototype. Begge disse målene dekkes av forskningsspørsmålene og representeres som resultater. Rapportens teorikapittel har til hensikt å gi leseren et faglig grunnlag for å forstå de neste seksjonene av oppgaven. Den vil både introdusere konsepter som er relevante for problemområdet og spesifikke verktøy som benyttes i prototypen. 

Resultat og diskusjon er inndelt i to deler, hvor den første delen omhandler studien av eksisterende verktøy og den andre delen omhandler prototypen.

Oppgaven har tre vedlegg. Disse presenterer prototypen med utfyllende detaljer som ikke er med i resultatkapittelet. Vedlegg C er tilført elektronisk. Dette består av prototypens kode og en demonstrasjonsvideo.

\section{Konkretisering av krav}
\label{ss:kkrav}
Oppgaveteksten gir føringer og krav til hva oppgaven skal undersøke og produsere. Hensikten med denne seksjonen er å systematisere og tilspisse oppgavens fokusområde. Listen med konkretiserte krav er basert på oppgaveteksten og innledende samtaler med faglig veileder. Kravene vil brukes som faste referansepunkter gjennom oppgaven, både når eksisterende tilnærmingsmetoder skal vurderes og i presentasjon og diskusjon rundt prototypen som skal utvikles.

Disse kravene er parafrasert fra oppgaveteksten: En komplett løsning skal

\begin{enumerate}
\item automatisk deployere virtuell infrastruktur basert på testkonfigurasjon.
\item ha testkonfigurasjoner i et versjonskontrollsystem med mulighet for tilbakerulling til tidligere versjon, utrulling av ny og fjerning av hele testinfrastrukturen.
\end{enumerate}

Disse kravene fremkommer ikke direkte i oppgaveteksten, men det er forstått implisitt at en løsning skal
\begin{enumerate}
\setcounter{enumi}{2}
\item være generell, ikke låst til spesifikke plattformer som kan deployeres.
\item kunne gjøre automatisert oppsett av nettverksutstyr.
\end{enumerate}

Krav 1-4 er ufravikelige. I tillegg, er det ønskelig å se på alternativer som kan
\begin{enumerate}
\setcounter{enumi}{4}
\item avbilde og deployere til fysiske datamaskiner.
\end{enumerate}

Denne listen forener egenskapene oppgaveteksten etterspør i løsninger som skal undersøkes med oppgavetekstens minimumskrav til prototypen som skal utvikles. De eksisterende løsningene som skal undersøkes behøver ikke nødvendigvis å oppfylle alle kravene hver for seg. Studien vil involvere løsninger hvor et eller flere av kravene er oppfylt, i tro på at sammensetninger av disse vil kunne oppfylle alle.

\section{Forskningsspørsmål}
\label{fs}
Forskningsspørsmålene er valgt på bakgrunn av problemområdet; det er behov for verktøy. Forrige seksjon har til hensikt å lede frem til og forenkle forskninsspørsmålene. Forskningsspørsmål 1 og 3 benytter seg av kravene og angir hva som etterspørres i det som skal studeres og utvikles. Svaret på disse spørsmålene vil utgjøre resultatets to hovedkomponenter: Studie og prototype. Forskningsspørsmål 2 skal skape et bindeledd mellom disse komponentene. Svaret på det vil anvende resultatene av forskningsspørsmål 1 og underbygge studien og samtidig være relevant for hvordan prototypen designes.


\begin{enumerate}
\item Hvilke tilgjengelige rammeverk og verktøy kan benyttes for å oppfylle kravene gitt i \ref{ss:kkrav}?
\item Hvordan kan de eksisterende tilnærmingsmetodene inndeles i typer arkitektur?
\item Hvordan bør en prototype designes for å oppfylle kravene gitt i \ref{ss:kkrav}?
\end{enumerate}




